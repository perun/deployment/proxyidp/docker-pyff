ARG PYFF_VERSION=""
ARG PYTHON_IMAGE="latest"
ARG CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX=""

FROM ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX:+/}python:${PYTHON_IMAGE}

ENV ENV LOGLEVEL INFO
ENV PIPELINE mdx.fd
ENV DATADIR /opt/pyff
ENV PUBLIC_URL "http://localhost:8080"
ARG PYFF_VERSION

LABEL version="2.0.0"
LABEL authors="Pavel Vyskocil <Pavel.Vyskocil@cesnet.cz>"
LABEL maintainer="Pavel Vyskocil <Pavel.Vyskocil@cesnet.cz>"

ADD pyffd.sh /pyffd.sh
ADD debug.ini /debug.ini
ADD warn.ini /warn.ini
ADD mdx.fd /mdx.fd

RUN apt update \
    && apt install -y libxml2 libxmlsec1 \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade pip \
    && pip3 install setuptools wheel python-pkcs11 pyFF${PYFF_VERSION:+==}${PYFF_VERSION}

EXPOSE 8080

RUN chmod a+x /pyffd.sh

ENTRYPOINT ["/pyffd.sh"]